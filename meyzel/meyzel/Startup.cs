﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(meyzel.Startup))]
namespace meyzel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
