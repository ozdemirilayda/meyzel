﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace meyzel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
      
  

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Konser()
        {
            ViewBag.Message = "Konserler";
            return View();
        }

        public ActionResult Tiyatro()
        {
            ViewBag.Message = "Tiyatrolar";
            return View();
        }

        public ActionResult Sinema()
        {
            ViewBag.Message = "Sinemalar";
            return View();
        }

        public ActionResult Lunapark()
        {
            ViewBag.Message = "LunaParklar";
            return View();
        }
    }
}