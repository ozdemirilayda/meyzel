﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using meyzel.Models;

namespace meyzel.Controllers
{
    [Authorize]
    public class MekanlarController : Controller
    {
        private MeyzelDBEntities db = new MeyzelDBEntities();
       
        // GET: Mekanlar
        public ActionResult Index()
        {
            return View(db.Mekanlar.ToList());
        }

        // GET: Mekanlar/Details/5
        public ActionResult Details(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mekanlar mekanlar = db.Mekanlar.Find(id);
            
            if (mekanlar == null)
            {
                return HttpNotFound();
            }
            ViewBag.yorumlar = db.Yorumlar.Where(s => s.MekanID == id).ToList();
            return View(mekanlar);
        }

        // POST: Yorumlars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(Yorumlar yorums)
        {
            if (ModelState.IsValid)
            {
                db.Yorumlar.Add(yorums);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Mekanlar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mekanlar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mekanID,mekanAdi,mekanAciklama,UlkeAdi,IlAdi,IlceAdi,mekanEtiket,mekanRaiting")] Mekanlar mekanlar)
        {
            if (ModelState.IsValid)
            {
                db.Mekanlar.Add(mekanlar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mekanlar);
        }

        // GET: Mekanlar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mekanlar mekanlar = db.Mekanlar.Find(id);
            if (mekanlar == null)
            {
                return HttpNotFound();
            }
            return View(mekanlar);
        }

        // POST: Mekanlar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "mekanID,mekanAdi,mekanAciklama,UlkeAdi,IlAdi,IlceAdi,mekanEtiket,mekanRaiting")] Mekanlar mekanlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mekanlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mekanlar);
        }

        // GET: Mekanlar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mekanlar mekanlar = db.Mekanlar.Find(id);
            if (mekanlar == null)
            {
                return HttpNotFound();
            }
            return View(mekanlar);
        }

        // POST: Mekanlar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mekanlar mekanlar = db.Mekanlar.Find(id);
            db.Mekanlar.Remove(mekanlar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
